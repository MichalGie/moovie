// 3rd party
import app from "firebase/app";
import "firebase/firebase-firestore";

// db
import firebaseConfig from "./firebaseConfig";

// utils
import { IMovie, IUserDocument } from './utils/interfaces';


class Firebase {
    db: app.firestore.Firestore
    constructor() {
        app.initializeApp(firebaseConfig);
        this.db = app.firestore();
    }

    createUser = (userDocument: IUserDocument) =>
    this.db
        .collection('users')
        .add(userDocument)

    setUserFavourites = (userEmail: string, favourites: Array<IMovie>) =>
    this.db
        .collection('users')
        .where("email", "==", userEmail)
        .limit(1)
        .get()
        .then(snapshot => {
            if(snapshot.empty) {
                // no user in db yet
                const userDocument: IUserDocument = {
                    email: userEmail,
                    favourites,
                }
                this.createUser(userDocument)
            }else{
                // update favourites
                this.db
                    .collection("users")
                    .doc(snapshot.docs[0].id)
                    .update({favourites});
            }  
        })
        .catch(err => console.error(err));

    getUserFavourites = (userEmail: string, callback: (success: boolean, favourites?: Array<IMovie>) => void) =>
    this.db
        .collection('users')
        .where("email", "==", userEmail)
        .limit(1)
        .get()
        .then(snapshot => {
            if (!snapshot.empty) {
                const favourites: Array<IMovie> = []
                const favs = snapshot.docs[0].data().favourites
                favs.forEach((fav: IMovie) => favourites.push({
                    Title:	fav.Title,
                    Year:	fav.Year,
                    imdbID:	fav.imdbID,
                    Type:	fav.Type,
                    Poster:	fav.Poster,
                }))
                callback(true, favourites)
          } else {
            callback(false)
        }})
        .catch(err => console.error(err));

    getMovie = async(imdbID: string, callback: (success: boolean, movie?: IMovie) => void) =>
    this.db
        .collection('movies')
        .where("imdbID", "==", imdbID)
        .limit(1)
        .get()
        .then(snapshot => {
            if (!snapshot.empty) {
                const movie: IMovie = {
                    Title:	snapshot.docs[0].data().Title,
                    Year:	snapshot.docs[0].data().Year,
                    imdbID:	snapshot.docs[0].data().imdbID,
                    Type:	snapshot.docs[0].data().Type,
                    Poster:	snapshot.docs[0].data().Poster,
                }
                callback(true, movie)
          } else {
            callback(false)
        }})
        .catch(err => console.error(err));
}

export default new Firebase();