// utils
import { IMovie } from './interfaces';


export function movieInFav(favouriteList: Array<IMovie>, movieImdbID: string): boolean{
      return Boolean(favouriteList.find((movie: IMovie) => movie.imdbID === movieImdbID))
}