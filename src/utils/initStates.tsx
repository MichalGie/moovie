// utils
import { IState, IMovie, IFbAuthState } from './interfaces';


export const initFbAuthState: IFbAuthState = {
    isAuthenticated: false,
    id: "",
    accessToken: "",
    name: "",
    email: "",
    picture: {data: {height: 0, width: 0, is_silhouette: false, url: "",}},
   
 }

export const initState: IState = {
    auth: initFbAuthState,
    movies: Array<IMovie>(),
    favourites: Array<IMovie>(),
 }

export const initMovie: IMovie = {
        Title:	"",
        Year:	"",
        imdbID:	"",
        Type:	"",
        Poster:	"",
 }