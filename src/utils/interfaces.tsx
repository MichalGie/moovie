// 3rd party
import { RouteComponentProps } from '@reach/router';
import { ReactFacebookLoginInfo } from 'react-facebook-login';


export interface IMovie {
    Title:	string
    Year:	string
    imdbID:	string
    Type:	string
    Poster:	string
}

export interface IMovieFull {
    Actors: string
    Awards: string
    BoxOffice: string
    Country: string
    DVD: string
    Director: string
    Genre: string
    Language: string
    Metascore: string
    Plot: string
    Poster: string
    Production: string
    Rated: string
    Ratings: Array<any>
    Released: string
    Response: string
    Runtime: string
    Title: string
    Type: string
    Website: string
    Writer: string
    Year: string
    imdbID: string
    imdbRating: string
    imdbVotes: string
}

export interface IState {
    auth?: IFbAuthState
    movies?: Array<IMovie>
    favourites?: Array<IMovie>
}

export interface IAction {
    type: string
    payload: IState,
}

export type Dispatch = React.Dispatch<IAction>

export interface IMoviePageProps extends RouteComponentProps {
    imdbID: string
}

export interface IFbAuthState extends ReactFacebookLoginInfo {
    isAuthenticated: boolean
    picture: {data: {
                      height: number
                      width: number
                      is_silhouette: boolean
                      url: string
                    }},
  }

export interface IUserDocument {
    email: string
    favourites: Array<IMovie>
}

export interface IMovieListProps {
    movieList: Array<IMovie>
    favouriteList: Array<IMovie>
    state: IState
    dispatch: Dispatch
  }

export interface IMovieProps {
    movie: IMovieFull
    favouriteList: Array<IMovie>
    state: IState
    dispatch: Dispatch
}

export interface IPanelProps {
    auth: IFbAuthState
    logout: () => void
}

export interface ILoginProps {
    auth: IFbAuthState
}