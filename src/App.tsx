import React from 'react';

// 3rd party
import { Link } from '@reach/router';

// db
import firebase from './Firebase';

// store
import { Store } from './stores/Store';
import { initFavouritesAction } from './stores/actions';

// components
import UserPanel from './components/UserPanel';

// utils
import { IMovie } from './utils/interfaces';
import { initFbAuthState } from './utils/initStates';


export default function App(props: any): JSX.Element{
  const { state, dispatch } = React.useContext(Store)
  
  // set user favourites movies in store
  React.useEffect(() => {
    state.auth.isAuthenticated && firebase.setUserFavourites(state.auth.email, state.favourites)
  }, [state.favourites]);

  // init user favourites movies from db
  React.useEffect(() => {
    const getUserFavourites = async(success: boolean, favourites?: Array<IMovie>) => {
      (success && favourites) && initFavouritesAction(state, dispatch, favourites)
    }
    firebase.getUserFavourites(state.auth.email, getUserFavourites)

    // TODO if not signed in
  }, [state.auth.isAuthenticated]);

  // TODO move to another place
  function logout(){
    return dispatch({
      type: 'SET_AUTH',
      payload: {auth: initFbAuthState}
    })
  }

  return(
      <div className='App'>
        <header className='navbar'>
          <div className='navbar-links'>
            <Link to='/'>Home</Link>
            <Link to='/favourites'>My favourites({state.favourites.length})</Link>
          </div>
          <UserPanel
            auth={state.auth}
            logout={logout}/>
        </header>
        <div className='app-container'>
          {props.children}
        </div>
      </div>
  );
}