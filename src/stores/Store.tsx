import React from 'react';

// utils
import { initState } from '../utils/initStates';
import { IState, IAction } from '../utils/interfaces';


export const Store = React.createContext<IState | any>(initState)

function reducer(state: IState, action: IAction): IState{
    switch(action.type){
        case 'FETCH_DATA':
            return {...state, movies: action.payload.movies}
        case 'SET_FAV':
            return {...state, favourites: action.payload.favourites}
        case 'INIT_FAV':
            return {...state, favourites: action.payload.favourites}
        case 'SET_AUTH':
            return {...state, auth: action.payload.auth}
        default:
            return state
    }
}

export function StoreProvider(props: any): JSX.Element{
    const [state, dispatch] = React.useReducer(reducer, initState)
    return <Store.Provider value={{state, dispatch}}>{props.children}</Store.Provider>
}