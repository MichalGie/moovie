// utils
import { movieInFav } from '../utils/utils';
import { IState, IMovie, Dispatch } from '../utils/interfaces';


export const fetchDataAction = async(dispatch: Dispatch, searchString: string) => {
    const URL = `http://www.omdbapi.com/?apikey=23985cce&s=${searchString}`
    const data = await fetch(URL)
    const dataJSON = await data.json()

    return dispatch({
      type: 'FETCH_DATA',
      payload: {movies: dataJSON.Search}
    })
  }

export const manageFavouritesAction = (state: IState, dispatch: Dispatch, movieImdbID: string) => {
    let newFavourites: Array<IMovie> = state.favourites ||  Array<IMovie>()

    if(state.favourites && (movieInFav(state.favourites, movieImdbID))){
      // remove the movie from fav
      newFavourites = newFavourites.filter(((movie: IMovie) => movie.imdbID !== movieImdbID))
    }else{
      // add the movie to fav
      if(state.movies){
        const favMovie: IMovie | undefined = state.movies.find((movie: IMovie) => movie.imdbID === movieImdbID)
        if(favMovie){
          newFavourites = [...newFavourites, favMovie]
        }
      }
    }

    return dispatch({
      type: 'SET_FAV',
      payload: {favourites: newFavourites}
    })
  }

export const initFavouritesAction = (state: IState, dispatch: Dispatch, favourites: Array<IMovie>) => {
  return dispatch({
    type: 'INIT_FAV',
    payload: {favourites}
  })
}