import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import App from './App';

// 3rd party
import { Router } from '@reach/router';

// pages
import HomePage from './pages/HomePage';
import FavPage from './pages/FavPage';
import MoviePage from './pages/MoviePage';
import NotFoundPage from './pages//NotFoundPage';
import LoginPage from './pages/LoginPage';

// store
import { StoreProvider } from './stores/Store';

// utils
import ProtectedRoute from './auth/Auth.ProtectedRoute';


ReactDOM.render(
        <StoreProvider>
            <Router>
                <App path='/'>
                    <HomePage path='/' />
                    <LoginPage path='/login' />
                    <NotFoundPage path='/notFound' default />
                    <ProtectedRoute component={MoviePage} path='movie/:imdbID' imdbID="tt0207201"/>
                    <ProtectedRoute component={FavPage} path='/favourites' />
                </App>
            </Router>
        </StoreProvider>,
        document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
