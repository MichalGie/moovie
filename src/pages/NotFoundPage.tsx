import React from 'react';

// 3rd party
import { RouteComponentProps } from '@reach/router';


export default function NotFoundPage(props: RouteComponentProps): JSX.Element{
  return(
    <div className='message'>Page not found</div>
  )
}