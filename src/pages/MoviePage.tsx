import React from 'react';

// db
import firebase from '../Firebase';

// store
import { Store } from '../stores/Store';

// components
import Movie from '../components/Movie';

// utils
import { initMovie } from '../utils/initStates';
import { IMovie, IMovieFull, IMoviePageProps } from '../utils/interfaces';


export default function MoviePage(props: IMoviePageProps): JSX.Element{
    const {state, dispatch} = React.useContext(Store)
    const [movie, setMovie] = React.useState<IMovie | IMovieFull | any>(initMovie)

    const getMovie = (success: boolean, movie?: IMovie): void => {
        // if data in db setState
        // else fetch movie data from API
        success ? setMovie(movie) : fetchMovieData()
    }

    const getMovieData = async() => {
        // try to get data from db
        console.log(props.imdbID)       
        if(props.imdbID){
            firebase.getMovie(props.imdbID, getMovie)
        }
    }

    const fetchMovieData = async() => {
        // fetch data from API
        const imdbID = props.imdbID
        const URL = `http://www.omdbapi.com/?apikey=23985cce&plot=full&i=${imdbID}`
        const data = await fetch(URL)
        const dataJSON = await data.json()

        const movie: IMovieFull = {
            Actors: dataJSON.Actors,
            Awards: dataJSON.Awards,
            BoxOffice: dataJSON.BoxOffice,
            Country: dataJSON.Country,
            DVD: dataJSON.DVD,
            Director: dataJSON.Director,
            Genre: dataJSON.Genre,
            Language: dataJSON.Language,
            Metascore: dataJSON.Metascore,
            Plot: dataJSON.Plot,
            Poster: dataJSON.Poster,
            Production: dataJSON.Production,
            Rated: dataJSON.Rated,
            Ratings: dataJSON.Ratings,
            Released: dataJSON.Released,
            Response: dataJSON.Response,
            Runtime: dataJSON.Runtime,
            Title: dataJSON.Title,
            Type: dataJSON.Type,
            Website: dataJSON.Website,
            Writer: dataJSON.Writer,
            Year: dataJSON.Year,
            imdbID: dataJSON.imdbID,
            imdbRating: dataJSON.imdbRating,
            imdbVotes: dataJSON.imdbVotes,
        }
         
        setMovie(movie)
        // saveInDBMovieData(movie)
      }
    
    React.useEffect(() => {
        getMovieData()
    }, [props.imdbID])
    
    return(
        <Movie
            movie={movie}
            favouriteList={state.favourites}
            state={state}
            dispatch={dispatch}/>
    );
}