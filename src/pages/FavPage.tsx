import React from 'react';

// 3rd party
import { RouteComponentProps } from '@reach/router';

// store
import { Store } from '../stores/Store';

// components
import MovieList from '../components/MovieList';


export default function FavPage(props: RouteComponentProps): JSX.Element{  
    const {state, dispatch} = React.useContext(Store)
    return(
        <React.Fragment>
            <div className='app-title'>Your Mooovies</div>
            {state.favourites.length !== 0 ?
                <div className='search-results'>
                    <MovieList
                    movieList={state.favourites}
                    favouriteList={state.favourites}
                    state={state}
                    dispatch={dispatch}/>
                </div> :
                <div className='message'>No movies in favourites</div>}
        </React.Fragment>
    );
}