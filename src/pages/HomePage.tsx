import React from 'react';

// 3rd party
import { RouteComponentProps } from '@reach/router';

// store
import { Store } from '../stores/Store';
import { fetchDataAction } from '../stores/actions';

// components
import MovieList from '../components/MovieList';


export default function HomePage(props: RouteComponentProps): JSX.Element{
  const {state, dispatch} = React.useContext(Store)

  function searchMovies(searchString: string): void{
    searchString.length >= 3 && fetchDataAction(dispatch, searchString)
  }

  return(
    <React.Fragment>
        <div className='app-title'>Mooovie</div>
        <div className='search-wrapper'>
          <input  type='text'
                  placeholder='What movie are you looking for?'
                  onChange={(e) => {searchMovies(e.target.value)}}/>
        </div>
        <div className='search-results'>
        <MovieList
              movieList={state.movies}
              favouriteList={state.favourites}
              state={state}
              dispatch={dispatch}/>
         </div>
    </React.Fragment>
  );
}