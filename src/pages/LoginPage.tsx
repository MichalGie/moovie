import React from 'react';

// 3rd party
import { RouteComponentProps, Redirect } from '@reach/router';

// store
import { Store } from '../stores/Store';

// components
import UserLogin from '../components/UserLogin';


export default function LoginPage(props: RouteComponentProps): JSX.Element{
  const {state} = React.useContext(Store)

  return(
    state.auth && state.auth.isAuthenticated ?
    <Redirect to="/" noThrow/> :
    <React.Fragment>
      <div className='message'>You need to be login to access this page.</div>
      <UserLogin auth={state.auth}/>
    </React.Fragment>
  )
}