import React from 'react'

// auth
import FbAuth from '../auth/Auth.Fb';

// utils
import { ILoginProps } from '../utils/interfaces';


export default function UserPanel(props: ILoginProps): JSX.Element{
    const {auth} = props

    return(
        <React.Fragment>
            
            {(auth && !auth.isAuthenticated) ?
            <div className="fb-auth"><FbAuth /></div>:
            <></>}
        </React.Fragment>
    )
}