import React from 'react'

// 3rd party
import { Link } from '@reach/router';

// store
import { manageFavouritesAction } from '../stores/actions';

// utils
import { movieInFav } from '../utils/utils';
import { IMovie, IMovieListProps } from '../utils/interfaces';


export default function MovieList(props: IMovieListProps): JSX.Element{
    return(
        <React.Fragment>
          <div className="movie-list-container">
            {props.movieList && props.movieList.map((movie: IMovie) => {
              return(
                <section key={`movie-id-${movie.imdbID}`}
                          className='movie'>
                      <div className='movie-poster'>
                        {movie.Poster !== "N/A" ?
                          <img src={movie.Poster} alt={`movie-poster${movie.Title}`}/> :
                          <div>No poster available</div>}
                      </div>
                      <div className='movie-actions'>
                        <Link   to={'#'}
                                className='button button-small'
                                onClick={() => {manageFavouritesAction(props.state, props.dispatch, movie.imdbID)}}>
                                {movieInFav(props.favouriteList, movie.imdbID) ? 'unFav' : 'Fav'}
                        </Link>
                        <Link   to={`/movie/${movie.imdbID}`}
                                className='button button-small'>
                              show
                        </Link>
                      </div>
                      <div className='movie-title'>{movie.Title}</div>
                </section>
              )
            })}
          </div>
        </React.Fragment>
    )
}