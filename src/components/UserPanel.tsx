import React from 'react'

// auth
import FbAuth from '../auth/Auth.Fb';

// utils
import { IPanelProps } from '../utils/interfaces';


export default function UserPanel(props: IPanelProps): JSX.Element{
    const {auth, logout} = props

    return(
        <React.Fragment>
            {(auth && !auth.isAuthenticated) ?
            <FbAuth /> :
            <>
                <section className='user-panel'>
                    <div>
                        <div>Your are logged as {auth.name}</div>
                        <div className='logout'>(<div onClick={logout}>logout</div>)</div>
                    </div>
                    <div className='user-img'>
                        <img src={auth.picture.data.url} alt={`facebook-profile-img`}/>
                    </div>
                </section>
            </>}
        </React.Fragment>
    )
}