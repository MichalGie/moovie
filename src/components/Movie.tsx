import React from 'react'

// store
import { manageFavouritesAction } from '../stores/actions';

// utils
import { movieInFav } from '../utils/utils';
import { IMovieProps } from '../utils/interfaces';


export default function Movie(props: IMovieProps): JSX.Element{
    const {movie, favouriteList, state, dispatch} = props
    console.log(movie)
    return(
        <React.Fragment>
        <section key={`movie-id-${movie.imdbID}`}
                className='movie-full'>
            <div className='movie-full-title'>{movie.Title}</div>

            <div className='movie-full-split-wrapper'>
                <div>
                    <div className='movie-full-poster'>
                        {movie.Poster !== "N/A" ?
                          <img src={movie.Poster} alt={`movie-poster${movie.Title}`}/> :
                          <div>No poster available</div>}
                    </div>
                    <div className='movie-full-favourite'>
                        <div    className='button'
                                onClick={() => {manageFavouritesAction(state, dispatch, movie.imdbID)}}>
                                {movieInFav(favouriteList, movie.imdbID) ? 'unFavourite' : 'Favourite'}
                        </div>
                    </div>
                </div>
                <div>
                    <div className='movie-full-plot'>{movie.Plot}</div>

                    <div className='movie-full-details-container'>
                        <div><div>Production:</div><div>{movie.Production}</div></div>
                        <div><div>Released:</div><div>{movie.Released}</div></div>
                        <div><div>Runtime:</div><div>{movie.Runtime}</div></div>
                        <div><div>Genre:</div><div>{movie.Genre}</div></div>
                        <div><div>Actors:</div><div>{movie.Actors}</div></div>
                        <div><div>Awards:</div><div>{movie.Awards}</div></div>
                    </div>
                </div>
            </div>
        </section>
        </React.Fragment>
    )
}