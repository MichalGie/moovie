import React from 'react';

// 3rd party
import { Redirect } from '@reach/router';

// store
import { Store } from '../stores/Store';


export default function ProtectedRoute(props: any): JSX.Element {
    const { state } = React.useContext(Store)

    console.log(props)
    if(state.auth.isAuthenticated){
        return <props.component {...props} />
    }
    return <Redirect from="" to="/login" noThrow />
};