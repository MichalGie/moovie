import React from 'react';

// 3rd party
import FacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login';

// store
import { Store } from '../stores/Store';


export default function FBAuth(props: any): JSX.Element{
  const {dispatch} = React.useContext(Store)

  const responseFacebook = (response: ReactFacebookLoginInfo): void => {
    return dispatch({
      type: 'SET_AUTH',
      payload: {auth: {...response, isAuthenticated: true}}
    })
  }

  return (
        <FacebookLogin
          appId="2365769573538198"
          autoLoad={true}
          fields="name,email,picture"
          callback={responseFacebook}
        />
    )
}