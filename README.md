# Project Title

Moovie - the search movies database app


### Live

Project is deployed to Heroku
```
https://moovie-tivix.herokuapp.com/
```

### IMPORTANT

Due to SSL certificate issue it might be needed to disable site protection in your browser

## Project highlights

* Movies data are fetched from OMDb API
* Users favourites movies are stored in Firebase
* Auth is provided with Facebook login
* App is implemented with React Hooks


## Running project

to run the project execute commands: npm install -> npm start


## Deployment

Add additional notes about how to deploy this on a live system


## Built With

* [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces
* [TypeScript](https://www.typescriptlang.org/) - Typed superset of JavaScript that compiles to plain JavaScript
* [OMDb API](http://www.omdbapi.com/) - Movie Database API


## TODO

* unit and integration tests
* RWD media queries


## Known bugs

* SSL certificate issue